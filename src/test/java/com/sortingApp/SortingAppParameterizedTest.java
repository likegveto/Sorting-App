/**
 * The SortingAppParameterizedTest class contains Parameterized test cases for the SortingApp class.
 * <p>
 * Parameterized Test contains several cases of testing
 * </p>
 */
package com.sortingApp;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppParameterizedTest {
    private String[] input;
    private int[] expected;

    public SortingAppParameterizedTest(String[] input, int[] expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new String[]{"3", "7", "1", "5"}, new int[]{1, 3, 5, 7}},
                {new String[]{"5"}, new int[]{5}},
                {new String[]{"10", "9", "8", "7", "6", "5", "4", "3", "2", "1"}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
        });
    }

    @Test
    public void testSortNumbers() {
        assertArrayEquals("Array should be sorted correctly.", expected, SortingApp.parseAndSort(input));
    }
}
