/**
 * The SortingAppTest class contains JUnit test cases for the SortingApp class.
 * <p>
 * The test cases cover various scenarios such as normal sorting, null input, no arguments,
 * too many arguments, and invalid number format.
 * </p>
 */
package com.sortingApp;


import org.junit.Test;
import java.util.Arrays;
import java.util.logging.Logger;

import static org.junit.Assert.assertArrayEquals;

public class SortingAppTest {

    private static final Logger logger = Logger.getLogger(SortingApp.class.getName());

    @Test
    public void testNormalSorting() {
        logger.info("testNormalSorting passed.");
        String[] input = {"3", "1", "2"};
        int[] expected = {1, 2, 3};
        assertArrayEquals(expected, SortingApp.parseAndSort(input));
    }


    @Test(expected = IllegalArgumentException.class)
    public void testNullInput() {
        logger.info("testNullInput passed.");
        SortingApp.parseAndSort(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNoArguments() {
        logger.info("testNoArguments passed.");
        SortingApp.parseAndSort(new String[0]);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooManyArguments() {
        logger.info("testTooManyArguments passed.");
        String[] input = new String[11];
        Arrays.fill(input, "1");
        SortingApp.parseAndSort(input);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidNumberFormat() {
        logger.info("testInvalidNumberFormat passed.");
        String[] input = {"1", "a", "3"};
        SortingApp.parseAndSort(input);
    }

    @Test
    public void testOneArguments() {
        assertArrayEquals(new int[]{1}, SortingApp.parseAndSort(new String[]{"1"}));
        logger.info("testOneArguments passed.");

    }

}
