package com.sortingApp;


import java.util.Arrays;
import java.util.logging.Logger;

public class SortingApp {
    /**
     * Sorts an array of integers in ascending order.
     *
     * @param args An array of integers to be sorted.
     * @return The sorted array of integers.
     */
    private static final Logger logger = Logger.getLogger(SortingApp.class.getName());

    public static int[] parseAndSort(String[] args) {
        if (args == null) {
            logger.info("No arguments provided.");
            throw new IllegalArgumentException();
        }
        if (args.length == 0) {
            logger.info("No arguments provided.");

            throw new IllegalArgumentException();
        }
        if (args.length > 10) {
            logger.info("Too many arguments.");
            throw new IllegalArgumentException();
        }

        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Invalid argument: " + args[i] + ". Please provide valid integer values.");
            }
        }
        Arrays.sort(numbers);
        return numbers;
    }

    public static void main(String[] args) {

        int[] sortedNumbers = parseAndSort(args);
        System.out.print("Sorted integers: ");
        for (int number : sortedNumbers) {
            System.out.print(number + " ");
        }
    }
}